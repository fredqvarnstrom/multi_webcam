# 1. Expand file system in `raspi-config` and update packages.
    
- Expand file system:
        
        sudo raspi-config
        
- Update packages:

        sudo apt-get update
    
# 2. Install ffmpeg on the Raspberry Pi #
    
    cd /usr/src
    sudo git clone git://git.videolan.org/x264
    cd x264
    sudo ./configure --host=arm-unknown-linux-gnueabi --enable-static --disable-opencl
    sudo make -j3
    sudo make install
    
    cd /usr/src
    sudo git clone https://github.com/FFmpeg/FFmpeg.git
    cd ffmpeg
    sudo ./configure --arch=armel --target-os=linux --enable-gpl --enable-libx264 --enable-nonfree
    sudo make -j3
    sudo make install
# 3. Install dependencies #
   
    sudo apt-get install fswebcam
    sudo pip3 install sparkpost
    
# 4. Copy source files to `/home/pi/multi_webcam`
    
    
# 5. Take photos from the multiple cameras
    
    fswebcam -r 1280x720 -d /dev/video0 /home/pi/multi_webcam/static/videos/001.jpg
    fswebcam -r 1280x720 -d /dev/bus/usb/001/014 001.jpg
We can change the device name as `/dev/video1, /dev/video2, ... /dev/video5`.
And also can change the resolution as well.


# 6. Create slide video from multiple image files #
    
    ffmpeg -framerate 10 -f image2 -i %03d.jpg -vf scale=500x500 -y out.gif

or

    ffmpeg -framerate 5 -loop 1 -i %03d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p -t 10 -y out.mp4 
    
NOTE: files must be 001.jpg, 002.jpg, ...

# 7. Enable auto-running.

Open the `/etc/rc.local` and insert this before `exit(0)`.
    
    (/usr/bin/python3 /home/pi/multi_webcam/camera_web_server.py)&