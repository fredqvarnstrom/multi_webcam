"""
definition of const
"""
PROGID = "gpiod"

RPI_IO_SERVER_IMPL = 'http'
# RPI_IO_SERVER_IMPL = 'jmux'

LOG_FILE = '/tmp/rpi-io-server.log'

POLLING_CYCLE = 5          #
REQUEST_TIMEOUT = 5        #
GPIO_EVENT_REQUEST_TIMEOUT = REQUEST_TIMEOUT

CONDITION_SLEEP = 0

# daughter board recognition pin numbers (i.e. not the chip numbers)
GPIO_PIN_HIGH = 1
GPIO_PIN_LOW = 0

# Board 0, Board 1
PIN_R_INT1 = 12
PIN_R_INT2 = 16
PIN_R_INT3 = 18
PIN_R_INT4 = 22
PIN_R_LDAC = 27
PIN_R_RDY_BSY = 29

PIN_BIT_0 = 7
PIN_BIT_1 = 11
PIN_BIT_2 = 13
PIN_BIT_3 = 15

# 0 - board0, 1 - board1, -1 no emulation,
BOARD_EMULATE = 1

BOARD_ID_0 = (0, 0, 0, 0)
BOARD_ID_1 = (0, 0, 0, 1)
BOARD_NOT_INSTALLED = (1, 1, 1, 1)

# Raspberry connector pin numbers (i.e. not the chip numbers)
BOARD_ID_PINS = (PIN_BIT_0, PIN_BIT_1, PIN_BIT_2, PIN_BIT_3)

# milestone 2
PULSE_DURATION_MAX = 4294967295
PULSE_GAP_MAX = 65535

DISTRIBUTION_FILE = "/etc/device_type"
DISTRIBUTION_INV = 0
DISTRIBUTION_RVS = 1
DISTRIBUTION_RIO = 2
DISTRIBUTION_OTH = 3

