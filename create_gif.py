from images2gif import writeGif
from PIL import Image
import os

base_dir = 'static/videos/'

file_names = sorted((fn for fn in os.listdir(base_dir) if fn.endswith('.jpg')))

print(file_names)
images = [Image.open(base_dir + fn) for fn in file_names]

size = (900, 900)
for im in images:
    im.thumbnail(size, Image.ANTIALIAS)

print(writeGif.__doc__)

filename = "my_gif.GIF"
writeGif(filename, images, duration=0.2)
