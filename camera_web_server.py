"""
I imagine it being..

SCREEN 1

'START!' button

SCREEN 2

Count-down 3, 2, 1....

SCREEN 3

"Pose!"

SCREEN 4

Displays the result looping MP4, with buttons below: "EMAIL TO ME" and "TRY AGAIN"

"""
from __future__ import print_function

import optparse
import signal
import sys

from logger import *
from httpserver import *


def get_options():
    """Return options obtained by parsing the command line."""
    levels = ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL')
    usage = "python %prog [options]"
    parser = optparse.OptionParser(usage=usage, description=__doc__)
    parser.add_option("-p", "--port",
                      dest="port",
                      metavar="PORT",
                      type="int",
                      default=80,
                      help="local TCP port [default: %default]")
    parser.add_option("-l", "--level",
                      dest="level",
                      metavar="LEVEL",
                      choices=levels,
                      default=levels[0],
                      help="logging level: " + ", ".join(levels) + " [default: %default]")
    parser.add_option("-q", "--quiet",
                      dest="quiet",
                      default=False,
                      action="store_true",
                      help="suppress console output")
    options, args = parser.parse_args()
    return options


def install_signal_handlers():
    def signal_term_handler(signal, frame):
        logger.critical("terminated by SIGTERM")
        sys.exit(1)

    signal.signal(signal.SIGTERM, signal_term_handler)


class CameraWebServer(object):
    """
    version 0.3
    """
    board = None

    @staticmethod
    def start(port):
        with HttpServer(port) as http:
            http.serve_forever()


def main():
    """

    :rtype: object
    """
    install_signal_handlers()

    options = get_options()
    set_loggers(options.level, options.quiet)

    try:
        # logger.info("starting version %s", GPIO.__version__)
        CameraWebServer.start(port=options.port)

    except KeyboardInterrupt as e:
        logger.critical("terminated by CTRL-C")

    except Exception as e:
        logger.exception(e)


if __name__ == "__main__":
    main()
