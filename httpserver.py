"""

"""
import copy
import os
import queue
from socketserver import ThreadingMixIn

import time

import datetime

from logger import logger
from httphandler import HttpHandler

import threading


try:
    import BaseHTTPServer
except ImportError:
    import http.server as BaseHTTPServer


class WebCamHandler(threading.Thread):

    _start_event = threading.Event()
    cmd = ''
    num = 0.0
    path = "/home/pi/multi_webcam/static/videos/"

    def __init__(self, num, path_num):
        super().__init__()
        self.num = num
        self.cmd = "fswebcam -r 500x500 -d /dev/video" + str(path_num) + " --no-banner " + self.path + "00" + str(num+1) + ".jpg"

    def run(self):
        self._start_event.clear()
        while True:
            if self._start_event.isSet():
                time.sleep(float(self.num) * 0.1)
                print("Taking a picture. -- ", datetime.datetime.now())
                print("CMD: " + self.cmd)
                os.system(self.cmd)
                self._start_event.clear()
                # Creating looping video
                time.sleep(1)

                # Iterating...
                if self.num == 1:
                    os.system("cp " + self.path + "002.jpg " + self.path + "010.jpg")
                elif self.num == 2:
                    os.system("cp " + self.path + "003.jpg " + self.path + "009.jpg")
                elif self.num == 3:
                    os.system("cp " + self.path + "004.jpg " + self.path + "008.jpg")
                elif self.num == 4:
                    os.system("cp " + self.path + "005.jpg " + self.path + "007.jpg")

                print("Finished time: ", datetime.datetime.now())
            else:
                time.sleep(0.1)

    def take_pic(self):
        self._start_event.set()


class HttpServer(ThreadingMixIn, BaseHTTPServer.HTTPServer,):
    """HTTP server that provides frontend for GPIO."""

    cam_list = [None for k in range(6)]

    def __init__(self, port):
        self.assign_devices()
        self.aa = None
        for i in range(6):
            self.cam_list[i].start()

        BaseHTTPServer.HTTPServer.__init__(self, ('', port), HttpHandler)

    def assign_devices(self):
        correct_list = ['CD971FEF', '493EC6DF', '1B3B2FEF', 'EB9A3FEF', '707DC6DF', '9A182FEF', ]

        sn_list = [get_serial(j) for j in range(6)]
        for i in range(6):
            try:
                path_num = sn_list.index(correct_list[i])
            except ValueError as e:
                print("Fatal Error: no found camera serial!!!", e)
                continue
            print("PATH of " + str(i) + ": " + str(path_num))
            tmp = WebCamHandler(i, path_num)
            self.cam_list[i] = tmp

    def __enter__(self):
        logger.debug("__enter__")
        return self

    def __exit__(self):
        logger.debug("__exit__")
        return False


def get_serial(num):
    pipe = os.popen("udevadm info --attribute-walk /dev/video" + str(num) + " | grep serial")
    data = pipe.read().strip().split("\n")[0]
    pipe.close()
    print("Serial of /dev/video/" + str(num) + ":")
    sn = data[16:-1]
    print(sn)
    return sn
